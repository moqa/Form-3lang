package ir.test.form3lang;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    TextView form ;
    EditText nameFamily , address ,mobile ;
    Button bt_en , bt_fa, bt_tr   ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        bind();
    }

    void bind(){
        form = findViewById(R.id.registration);
        nameFamily = findViewById(R.id.name_family);
        address = findViewById(R.id.address);
        mobile = findViewById(R.id.mobile);
        findViewById(R.id.bt_en).setOnClickListener(this);
        findViewById(R.id.bt_fa).setOnClickListener(this);
        findViewById(R.id.bt_tr).setOnClickListener(this);

    }
    @Override
    public void onClick(View v) {
        if(v.getId()==R.id.bt_en){
           setValueEn();
        }else if(v.getId()==R.id.bt_fa) {
           setValueFa();
        }else{
            setValueTr();
        }
    }
    void setValueEn(){
        form.setText(getString(R.string.regen));
        nameFamily.setText(getString(R.string.family_en));
        address.setText(getString(R.string.address_en));
        mobile.setText(getString(R.string.mobile_en));

    }
    void setValueFa(){
        form.setText(getString(R.string.registration));
        nameFamily.setText(getString(R.string.name_family));
        address.setText(getString(R.string.address));
        mobile.setText(getString(R.string.mobile));

    }
    void setValueTr(){
       form.setText(getString(R.string.reg_tr));
       nameFamily.setText(getString(R.string.family_tr));
       address.setText(getString(R.string.address_tr));
       mobile.setText(getString(R.string.mobile_tr));
    }
}
